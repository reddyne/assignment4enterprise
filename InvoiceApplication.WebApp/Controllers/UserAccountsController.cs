﻿using InvoiceApplication.Domain;
using InvoiceApplication.Domain.Entities;
using InvoiceApplication.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceApplication.WebApp.Controllers
{
    /// <summary>
    /// Used to interact with Users
    /// </summary>
    public class UserAccountsController : Controller
    {
        /// <summary>
        /// Landing Page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Start()
        {
            return View();
        }

        /// <summary>
        /// Checks authorization of manager / user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Start(User user)
        {
            if (ModelState.IsValid)
            {
                //grab post variables and set as strings
                string username = user.UserName;
                string password = user.Password;

                //new db object
                DBrepository db = new DBrepository();

                //create user out of input
                User newUser = new User(username, password, 0);

                //set user id for manager logins
                if (newUser.UserName == "Riley")
                {
                    newUser.UserID = 100;
                }
                else if (newUser.UserName == "Amanda")
                {
                    newUser.UserID = 101;
                }

                //reverse password string
                StringReverser sr = new StringReverser();
                string reversedPassword = sr.Reverse(password);

                //compare password to username
                if (username.ToLower() == reversedPassword.ToLower())
                {
                    if (Request["loginOption"] == "Manager Login")
                    {
                        //confirm is user is a manager
                        bool isManager = db.ValidateUser(newUser);

                        //set manager in session to be true
                        if (isManager)
                        {
                            Session["isManager"] = true;
                        }

                        //redirect to manager login if succesful
                        if (isManager)
                        {
                            return RedirectToAction("LoginSuccess", "Invoice", user);
                        }
                        else
                        {
                            //inform user they dont have mod priveleges
                            ModelState.AddModelError(string.Empty, "You do not have manager priveleges");
                            return View();
                        }
                    }
                    //return non-mod to login page
                    return RedirectToAction("LoginSuccess", "Invoice", user);
                }
            }
            //return to login page if model not valid
            return View();
        }
    }
}