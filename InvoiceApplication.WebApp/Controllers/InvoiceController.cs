﻿using InvoiceApplication.Domain.Entities;
using InvoiceApplication.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InvoiceApplication.WebApp.Controllers
{
    /// <summary>
    /// Controller to be used to interact with Invoices
    /// </summary>
    public class InvoiceController : Controller
    {

        private IInvoiceCatalog _invoiceCatalog;

        /// <summary>
        /// ActionLink used as gateway to InvoiceController
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult LoginSuccess(User user)
        {
            return RedirectToAction("InvoiceManager", "Invoice", user);
        }

        /// <summary>
        /// Used for main menu
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ActionResult InvoiceManager(User user)
        {
            //redirect to respective actions
            switch (Request["userChoice"])
            {
                case "Add an Invoice":
                    return RedirectToAction("AddInvoice", "Invoice");
                case "Review Receivables":
                    return RedirectToAction("ReviewReceivables", "Invoice");
            }
            return View(user);
        }

        /// <summary>
        /// Redirects user with invoice as model
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditInvoice(Invoice invoice)
        {
            return View("AddInvoice", invoice);
        }

        /// <summary>
        /// Redirects user with invoice as model
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddInvoice()
        {
            return View();
        }

        /// <summary>
        /// Instantiates db object in order to interact with db.
        /// Functions according to user input. 
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddInvoice(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                DBrepository db = new DBrepository();

                switch (Request["userSelection"])
                {
                    case "Submit Invoice":
                        //add the invoice to db
                        db.SaveInvoice(invoice);
                        //redirect to invoice manager
                        return RedirectToAction("InvoiceManager", "Invoice");
                    case "Save Invoice":
                        //update invoice
                        db.SaveInvoice(invoice);
                        //redirect to invoice manager
                        return RedirectToAction("InvoiceManager"); ;
                    case "Mark as Paid":
                        //update and mark as paid
                        db.MarkAsPaid(invoice);
                        //redirect to invoice manager
                        return RedirectToAction("ReviewReceivables"); ;
                }
            }
            //default return view
            return View(invoice);
        }

        public ActionResult ReviewReceivables()
        {
            //load db
            DBrepository db = new DBrepository();

            //load invoices
            IEnumerable<Invoice> invoices = db.InvoiceStore;

            //return view with invoices loaded from db
            return View(invoices);
        }
    }
}