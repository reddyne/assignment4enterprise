﻿using InvoiceApplication.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Persistence
{
    interface IUserRepository
    {
        IEnumerable<User> UserStore { get; }

        bool ValidateUser(User user);
    }
}
