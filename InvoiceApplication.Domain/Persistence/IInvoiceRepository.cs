﻿using InvoiceApplication.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Persistence
{
    /// <summary>
    /// Interface for Invoices interacting with db
    /// </summary>
    interface IInvoiceRepository
    {
        IEnumerable<Invoice> InvoiceStore { get; }

        void SaveInvoice(Invoice invoice);

        void MarkAsPaid(Invoice invoice);
    }
}
