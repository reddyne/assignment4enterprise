﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceApplication.Domain;
using InvoiceApplication.Domain.Entities;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace InvoiceApplication.Domain.Persistence
{
    public class DBrepository : IInvoiceRepository, IUserRepository
    {

        private StoreDbContext _dbContext;

        public DBrepository()
        {
            _dbContext = new StoreDbContext();
        }

        public IEnumerable<Invoice> InvoiceStore
        {
            get
            {
                return _dbContext.Invoices;
            }
        }

        public IEnumerable<User> UserStore
        {
            get
            {
                return _dbContext.Users;
            }
        }

        public void MarkAsPaid(Invoice invoice)
        {
            Invoice invoiceEntity = _dbContext.Invoices.Find(invoice.InvoiceNo);

            invoice.Paid = true;

            invoiceEntity.Change(invoice);

            //apply the changes (add or update) to the database
            try
            {
                _dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException upEx)
            {
                Console.WriteLine(upEx);
            }
        }

        public void SaveInvoice(Invoice invoice)
        {
            //check if this is a new invoice
            if (invoice.InvoiceNo == 0)
            {
                //add the invoice to the database
                _dbContext.Invoices.Add(invoice);
            }
            else
            {
                //change the invoice IN the database. How?
                //obtain the invoice FROM the database
                Invoice invoiceEntity = _dbContext.Invoices.Find(invoice.InvoiceNo);

                //change the invoice entity itself
                invoiceEntity.Change(invoice);
            }

            //apply the changes (add or update) to the database
            try
            {
                _dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException upEx)
            {
                Console.WriteLine(upEx);
            }
        }

        /// <summary>
        /// Ensures user is allowed to access manager pages
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool ValidateUser(User user)
        {
            User userEntity = _dbContext.Users.Find(user.UserID);

            if (user.Password == userEntity.Password)
            {
                return true;
            }

            else { return false; }
        }
    }
}
