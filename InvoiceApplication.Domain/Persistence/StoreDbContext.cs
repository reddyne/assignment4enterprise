﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using InvoiceApplication.Domain.Entities;

namespace InvoiceApplication.Domain.Persistence
{
    class StoreDbContext : DbContext
    {
        public StoreDbContext(): base("StoreDbConnection")
        {
        }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
