﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Entities
{
    public interface IUserCatalog
    {
        IEnumerable<User> UserList { get; }

        bool ValidateUser(User user);
    }
}
