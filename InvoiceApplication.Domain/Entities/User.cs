﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Entities
{
    public class User
    {
        private int v;

        [Key]
        public int UserID { get; set; }

        /// <summary>
        /// Name of the user
        /// </summary>
        [Required(ErrorMessage = "Please enter your name")]
        public string UserName { get; set; }

        /// <summary>
        /// Password of the user
        /// </summary>
        [Required(ErrorMessage = "Please enter your password")]
        public string Password { get; set; }

        /// <summary>
        /// Whether user is a manager or not
        /// </summary>
        public byte Type { get; set; }

        public string UserChoice { get; set; }

        public User(string name, bool isManager)
        {
            this.UserName = name;
            if (isManager)
            {
                Type = 1;
            }
            else { Type = 0; }
        }

        public User(string name, bool isManager, string password, int id)
        {
            this.UserName = name;
            this.Password = password;
            this.UserID = id;
            if (isManager)
            {
                Type = 1;
            }
            else { Type = 0; }
        }

        //default constructor for anonymous objects
        public User() { }

        public User(string username, string password, int id)
        {
            UserName = username;
            Password = password;
            UserID = id;
        }

        internal void Change(User user)
        {
            this.Password = user.Password;
            this.Type = user.Type;
            this.UserName = user.UserName;
            this.UserChoice = user.UserChoice;
        }
    }
}
