﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Entities
{
    public enum CurrencyType: byte
    {
        CAD, 
        US,
        EUR
    }

    public class Invoice 
    {
        [Key]
        public int InvoiceNo { get; set; }

        [Required]
        public string ClientName { get; set; }

        [Required]
        public string ClientAddress { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime ShipmentDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime PaymentDate { get; set; }

        [Required]
        public string ProductName { get; set; }

        [Required]
        public int ProductQuantity { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public CurrencyType Currency { get; set; }

        public bool Paid { get; set; }

        /// <summary>
        /// Changes object to the input one.
        /// </summary>
        /// <param name="invoice"></param>
        public void Change(Invoice invoice)
        {
            this.ClientName = invoice.ClientName;
            this.ClientAddress = invoice.ClientAddress;
            this.Currency = invoice.Currency;
            this.Paid = invoice.Paid;
            this.ProductName = invoice.ProductName;
            this.ProductQuantity = invoice.ProductQuantity;
            this.ShipmentDate = invoice.ShipmentDate;
            this.UnitPrice = invoice.UnitPrice;
            this.PaymentDate = invoice.PaymentDate;
        }

        public double GetTotal()
        {
            return ((Double)this.UnitPrice * this.ProductQuantity);
        }

    }
}
