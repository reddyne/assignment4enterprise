﻿using InvoiceApplication.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Entities
{
    class UserCatalog : IUserCatalog
    {
        private IUserRepository _repository;

        public IEnumerable<User> UserList { get; }

        public UserCatalog(IUserRepository repository)
        {
            _repository = repository;

            this.UserList = _repository.UserStore;
        }

        public bool ValidateUser(User user)
        {
            return _repository.ValidateUser(user);
        }
    }
}
