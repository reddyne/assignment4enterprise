﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain.Entities
{
    public interface IInvoiceCatalog
    {
        IEnumerable<Invoice> invoiceList { get; }

        void saveInvoice(Invoice invoice);
    }
}
