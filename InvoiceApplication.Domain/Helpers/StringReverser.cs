﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceApplication.Domain
{
    public class StringReverser
    {
        public string Reverse(string inString)
        {
            char[] charArray = inString.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
