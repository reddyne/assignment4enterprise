﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace InvoiceApplication.UnitTests
{
    [TestClass]
    public class InvoiceUnitTest
    {
        private IKernel _diKernel = new StandardKernel();

        public InvoiceUnitTest() { }

        [TestMethod]
        public void CalculateSubtotal()
        {
            //step 1: arrange

            //step 2: assert

            //step 3: act
        }

        [TestMethod]
        public void CalculateTaxes()
        {
            //step 1: arrange

            //step 2: assert

            //step 3: act
        }

        [TestMethod]
        public void CalculateTotal()
        {
            //step 1: arrange

            //step 2: assert

            //step 3: act
        }
    }
}
